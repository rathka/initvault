#!/usr/bin/env dash
set -x

. "$(dirname $0)/test.env" || exit 1
. "$(dirname $0)/../initcpio/hook.sh" || exit 1

UUID="UUID:7223bbc9-d825-4a25-a693-c877cc06cd9d"
EXEC="EXEC:tst.sh"
SSH="SSH:file.cfg"

vault="${UUID},${EXEC},subvol=/init"
cryptsetup="readonly,keyfile:install,CMD:open"
run_hook

# vim: set ft=sh ts=4 sw=4 et:
