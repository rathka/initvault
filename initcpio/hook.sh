#!/usr/bin/ash

run_hook() {
    local OFS="${IFS}"
    local vault_dev=''
    local vault_run=''
    local vault_opts='ro'
    local vault_mnt="$(mktemp -d)"
    local crypt_cmd='luksOpen'
    local crypt_args='--readonly'

    fail() {
        err "unlocking '${1}' failed!"
        msg "system will poweroff in 30 seconds"
        sleep 30
        poweroff -f
    }

    parse_vault_args() {
        [ -n "${vault}" ] || return 1
        IFS=','; set ${vault}
        for param in $@; do
            IFS=':'; set ${param}
            key=$1
            shift
            case ${key} in
            UUID)
                vault_dev="/dev/disk/by-uuid/$*"
                ;;
            EXEC)
                vault_run="$*"
                ;;
            *) 
                vault_opts="${vault_opts},${param}"
                ;;
            esac
        done
        IFS="${OFS}"
    }
    
    parse_cryptsetup_args() {
        [ -n "${cryptsetup}" ] || return 1
        crypt_args=''
        IFS=','; set ${cryptsetup}
        for param in $@; do
            IFS=':'; set ${param}
            arg=$1
            shift
            case ${arg} in
            CMD)
                crypt_cmd=$*
                ;;
            keyfile)
                [ -f $* ] || continue
                crypt_args="${crypt_args} --${arg} $*"
                ;;
            *)
                crypt_args="${crypt_args} --${arg} $*"
                ;;
            esac
        done
        IFS="${OFS}"
    }
    
    open_vault() {
        modprobe -a -q dm-crypt >/dev/null 2>&1
        poll_device "${vault_dev}" &&
            [ -b "${vault_dev}" ] && cd "$(dirname "${vault_dev}")" &&
            cryptsetup ${crypt_args} "${crypt_cmd}" "$(basename "${vault_dev}")" vault &&
            mount -o "${vault_opts}" /dev/mapper/vault "${vault_mnt}" ||
            fail "${vault_dev}"
    }

    run_script() {
        cd "${vault_mnt}" && [ -x "${vault_run}" ] || return 1
        /bin/sh "${vault_run}"
    }

    close_vault() {
        cd && umount "${vault_mnt}" && rmdir "${vault_mnt}" && cryptsetup close vault
    }

    parse_vault_args || return 0
    parse_cryptsetup_args
    open_vault
    run_script
    close_vault

    unset fail parse_vault_args parse_cryptsetup_args open_vault run_script close_vault
}
