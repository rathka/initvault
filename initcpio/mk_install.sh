#!/usr/bin/env sh


[ -f "$1" ] || exit 1
sed '/help().*/Q' "$1"

cat <<EOF
help() {
  cat <<HELPEOF
This hook needs some description.
HELPEOF
}

# vim: set ft=sh ts=4 sw=4 et:
EOF
