#!/bin/sh

cd $(dirname $0)
[ -d '.aur' ] && ( rm -r .aur || exit 1 )
mkdir .aur; cd .aur
find ../*/* -type f -exec ln {} ./ \;
ln -s ../README.md ./
cryptsetup_install='/usr/lib/initcpio/install/encrypt'
pkgbuild=$(mktemp)

cat > ${pkgbuild} << EOF
# Maintainer: Rasmus Thystrup Karstensen <rathka at gmail dot com>
pkgname='initvault'
pkgver='1.0'
pkgrel='1'
pkgdesc='Scripts for unlocking encrypted partitions from initramfs'
arch=(any)
license=(GPL3)
depends=(cryptsetup)
source=('README.md'
        'hook.sh'
        'mk_install.sh')
sha256sums=('skip'
            'skip'
            'skip')

package() {
  mkdir -p \${pkgdir}/usr/lib/initcpio/install
  ./mk_install.sh "${cryptsetup_install}" > \${pkgdir}/usr/lib/initcpio/install/vault
  install -T -D -m 644 README.md \${pkgdir}/usr/share/doc/dmcrypt/README
  install -T -D -m 644 hook.sh \${pkgdir}/usr/lib/initcpio/hooks/vault
}
EOF

mv ${pkgbuild} PKGBUILD
updpkgsums
makepkg $@
